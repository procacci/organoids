## Pipeline for the analysis of sci-ATAC-seq

### Reads preprocessing, Mapping

---

### Barcode calling, trimming, mapping, deduplication

#### Input

```
READ1=/g/furlong/STOCKS/Data/Assay/sequencing/2022/2022-09-01-HNMYKBGXM/HNMYKBGXM_SS196_22s003900-1-1_Secchia_lane122s003900_1_sequence.txt.gz
READ2=/g/furlong/STOCKS/Data/Assay/sequencing/2022/2022-09-01-HNMYKBGXM/HNMYKBGXM_SS196_22s003900-1-1_Secchia_lane122s003900_2_sequence.txt.gz
```

#### Script

```
[seneca] $ sh /g/furlong/procaccia/src/sh/jobs/sci_atac_preprocessing.sh
```

#### Output

```
/g/furlong/procaccia/organoids/analysis/preproc/Cusanovic/bam/SS196_simo_atac_organoids_trimmed_paired_filtered_dedup.{bam,bai}
```

#### Notes

The files that are being used were produced by stefano and are in `/g/tier2/furlong/secchia/organoids/SS196/bam/SS196.{true.nodups,Cardioids,EMLOC,HFO}.{bam,bai}`
This script was run on SENECA (reproducibility for this reason might be compromised). It is a script composed of multiple sub-scripts from Stefano Secchia pipeline for sci-ATAC-seq preprocessing.
---

### Peak calling

---

#### Input

```
/g/tier2/furlong/secchia/organoids/SS196/bam/SS196.{true.nodups,Cardioids,EMLOC,HFO}.{bam,bai}
```

#### Script

macs2 with parameters to decide

#### Output

analysis/MACS2/*_summits.bed

Or, if merged:

analysis/MACS2/SS196_merged*.bed

### Count matrix calculation

---

#### Input

- BAM file
- Peak calling output

#### Script

```
python2.7 66_sciATAC_meso/src/fly-atac_SS/sc_atac_window_counter_SS.py [standard parameters]

```
#### Output

analysis/matrix/Cusanovic/*.tab

### Fragment file construction

---

#### Input
- BAM file

#### Script 

`/src/sh/getFragments.sh -p 8` 

Which runs the sinto CLI with parameters
```
--barcode_regex "[^:]*" \
--collapse_within \
--nproc $NPROC \
--use_chrom "(?i)^[1-9XY]*"
```

Then compresses everything with `bgzip` and indexes the files with `tabix`

#### Output



### Seurat Object construction

---

#### Input
- matrix files analysis/matrix/Cusanovic/*.tab
- fragment

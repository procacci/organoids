FROM rocker/r-ubuntu:18.04

RUN /usr/bin/R --vanilla -e 'install.packages("devtools",repos="http://cran.rstudio.com/")'
RUN /usr/bin/R --vanilla -e 'install.packages("optparse",repos="http://cran.rstudio.com/")'
RUN /usr/bin/R --vanilla -e 'if (!requireNamespace("BiocManager",quietly=TRUE)) install.packages("BiocManager",repos="http://cran.rstudio.com/")'
RUN /usr/bin/R --vanilla -e 'BiocManager::install()'
RUN /usr/bin/R --vanilla -e 'BiocManager::install("Signac")'
RUN /usr/bin/R --vanilla -e 'BiocManager::install(c("TxDb.Hsapiens.UCSC.hg38.knownGene", "org.Hs.eg.db",\
                       "ChIPseeker", "fastcluster", "ComplexHeatmap", "doMC"))'
RUN /usr/bin/R --vanilla -e 'devtools::install_github("aertslab/cisTopic")'
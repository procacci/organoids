from snakemake.utils import Paramspace
import pandas as pd

configfile: "profile/config_analysis.yaml"
paramspace = Paramspace(pd.read_csv("profile/params.tsv", sep="\t"),filename_params=["resolution"])

GROUPS=["Cardioids", "EMLOC", "HFO"]

wildcard_constraints:
  peakset="(merged_std|merged_custom)",
  protocol="(_Cardioids_cells|_HFO_cells|_EMLOC_cells|)"
  
def get_resolution(wildcards):
    id = ''.join([wildcards.peakset, wildcards.protocol]) 
    res = config[f'{id}']['res']
    return res

rule all:
  input: 
    # expand("/g/furlong/procaccia/organoids/analysis/seurat/QC/SS196_{peakset}_QC_vlnplots_{type}.png", type=["all", f"by_protocol"], peakset=config["peakset"]),
    # expand("/g/furlong/procaccia/organoids/analysis/seurat/SS196_{peakset}_filtered_Seurat.Rds", peakset=config["peakset"]),
    expand("/g/furlong/procaccia/organoids/analysis/seurat/SS196_{peakset}_filtered_Seurat{protocol}.Rds", protocol=["_Cardioids_cells", "_HFO_cells", "_EMLOC_cells", ""], 
    peakset=config["peakset"]),
    expand("/g/furlong/procaccia/organoids/analysis/seurat/plots/SS196_{peakset}_filtered_Seurat{protocol}_depth_cor.png",
    protocol=['_Cardioids_cells', "_HFO_cells", "_EMLOC_cells", ""], peakset=config["peakset"]),
    # expand("/g/furlong/procaccia/organoids/analysis/seurat/plots/silhouette/{peakset}_peakset{protocol}/SS196_{peakset}_Seurat{protocol}_Silhouette_{params}.png",
    # protocol=['_Cardioids_cells', "_HFO_cells", "_EMLOC_cells", ""], peakset=config["peakset"], params=paramspace.instance_patterns),
    expand("/g/furlong/procaccia/organoids/analysis/seurat/plots/silhouette/{peakset}_peakset{protocol}/SS196_{peakset}_Seurat{protocol}_Silhouette_{params}.png",
    protocol=['_Cardioids_cells', "_HFO_cells", "_EMLOC_cells",""], peakset=config["peakset"], params=paramspace.instance_patterns),
    expand("/g/furlong/procaccia/organoids/analysis/seurat/UMAP/SS196_{peakset}_UMAP_Seurat{protocol}.Rds", 
    protocol=["_Cardioids_cells", "_HFO_cells", "_EMLOC_cells", ""], peakset=config["peakset"]),
    expand("/g/furlong/procaccia/organoids/analysis/seurat/plots/SS196_{peakset}_filtered_Seurat_UMAP_protocol.png", peakset=config["peakset"]),
    expand("/g/furlong/procaccia/organoids/analysis/seurat/gene_activity/SS196_{peakset}_gene_activity_Seurat{protocol}.Rds",
    protocol=["_Cardioids_cells", "_HFO_cells", "_EMLOC_cells", ""], peakset=config["peakset"]),
    expand("/g/furlong/procaccia/organoids/analysis/seurat/gene_activity/SS196_{peakset}_clusters_Seurat{protocol}_gene_activity_markers.tsv",
    protocol=["_Cardioids_cells", "_HFO_cells", "_EMLOC_cells", ""], peakset=config["peakset"]),
    expand("/g/furlong/procaccia/organoids/analysis/seurat/cistopic/SS196_{peakset}_cisTopicObject{protocol}.Rds",
    protocol=["_Cardioids_cells", "_HFO_cells", "_EMLOC_cells", ""], peakset=config["peakset"])
    

rule load_seurat:
  input:
    directory="/g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/{peakset}"
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis.yaml"
  output:
    "/g/furlong/procaccia/organoids/analysis/seurat/SS196_{peakset}_Seurat.Rds"
  log:
    "/g/furlong/procaccia/organoids/analysis/logs/load_seurat_{peakset}.log"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  params:
    genome="hg38",
    metadata=f"/g/furlong/procaccia/organoids/analysis/SS196_metadata.tsv",
    fragments=f"/g/furlong/procaccia/organoids/analysis/fragments/SS196_all_fragments.bed.gz"
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/load_Seurat_ATAC.R --dir {input.directory} --meta {params.metadata} --fragments {params.fragments} --genome {params.genome} --out {output} \
    &> {log}"


rule count_frags:
  input:
    seurat=ancient("/g/furlong/procaccia/organoids/analysis/seurat/SS196_{peakset}_Seurat.Rds")
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis.yaml"
  output:
    "workflow/flags/flag_count_frags_{peakset}.flag"
  log:
    "/g/furlong/procaccia/organoids/analysis/logs/count_frags_{peakset}.log"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  params:
    fragments="/g/furlong/procaccia/organoids/analysis/fragments/SS196_all_fragments.bed.gz",
    output="/g/furlong/procaccia/organoids/analysis/seurat/SS196_{peakset}_Seurat.Rds"
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/count_fragments.R --Seurat {input.seurat} --fragments {params.fragments} --out {params.output} &> {log} && touch {output}"

rule calc_FRiP:
  input:
    seurat=ancient(rules.count_frags.params.output),
    flag=rules.count_frags.output
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis.yaml"
  output:
    "workflow/flags/flag_calc_FRiP_{peakset}"
  log:
    "/g/furlong/procaccia/organoids/analysis/logs/calc_FRiP_{peakset}.log"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  params:
    output="/g/furlong/procaccia/organoids/analysis/seurat/SS196_{peakset}_Seurat.Rds"
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/calculate_FRiP.R --Seurat {input.seurat} --out {params.output} &> {log} && touch {output}"

rule calc_QC:
  input:
    seurat=ancient(rules.calc_FRiP.params.output),
    flag=rules.calc_FRiP.output
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis.yaml"
  output:
    "workflow/flags/flag_calc_QC_{peakset}.flag"
  log:
    "/g/furlong/procaccia/organoids/analysis/logs/calc_QC_{peakset}.log"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  params:
    output="/g/furlong/procaccia/organoids/analysis/seurat/SS196_{peakset}_Seurat.Rds"
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/calculate_QC.R --Seurat {input.seurat} --out {params.output} &> {log} && touch {output}"


rule plot_QC:
  input:
    seurat=ancient(rules.calc_QC.params.output),
    flag=rules.calc_QC.output
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis.yaml"
  output:
    expand("/g/furlong/procaccia/organoids/analysis/seurat/QC/SS196_{{peakset}}_QC_vlnplots_{type}.png", type=["all", f"by_protocol"])
  log:
    "/g/furlong/procaccia/organoids/analysis/logs/plot_QC_{peakset}.log"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  params:
    prefix="/g/furlong/procaccia/organoids/analysis/seurat/QC/SS196_{peakset}_",
    group=config["GROUP"]
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/plot_QC.R --Seurat {input.seurat} --prefix {params.prefix} --group {params.group} > {log} 2>&1"

rule filter_QC:
  input:
    seurat=ancient(rules.plot_QC.input.seurat),
    flag=rules.plot_QC.output
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis.yaml"
  output:
    output="/g/furlong/procaccia/organoids/analysis/seurat/SS196_{peakset}_filtered_Seurat.Rds",
    flag="workflow/flags/flag_filter_QC_{peakset}.flag"
  log:
    "/g/furlong/procaccia/organoids/analysis/logs/filter_QC_{peakset}.log"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  params:
    query=config["filtering"]
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/filter_QC.R {input.seurat} {params.query} {output.output} > {log} 2>&1 && touch {output.flag}"

rule split_Cardioids:
  input:
    seurat="/g/furlong/procaccia/organoids/analysis/seurat/SS196_{peakset}_filtered_Seurat.Rds"
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis.yaml"
  output:
    "/g/furlong/procaccia/organoids/analysis/seurat/SS196_{peakset}_filtered_Seurat_Cardioids_cells.Rds"
  log:
    "/g/furlong/procaccia/organoids/analysis/logs/split_Cardioids_cells_{peakset}.log"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  params:
    meta="protocol",
    group="Cardioids"
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/filter_metadata.R {input.seurat} {params.meta} {params.group} {output} > {log} 2>&1"

rule split_EMLOC:
  input:
    seurat="/g/furlong/procaccia/organoids/analysis/seurat/SS196_{peakset}_filtered_Seurat.Rds"
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis.yaml"
  output:
    "/g/furlong/procaccia/organoids/analysis/seurat/SS196_{peakset}_filtered_Seurat_EMLOC_cells.Rds"
  log:
    "/g/furlong/procaccia/organoids/analysis/logs/split_EMLOC_cells_{peakset}.log"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  params:
    meta="protocol",
    group="EMLOC"
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/filter_metadata.R {input.seurat} {params.meta} {params.group} {output} > {log} 2>&1"

rule split_HFO:
  input:
    seurat="/g/furlong/procaccia/organoids/analysis/seurat/SS196_{peakset}_filtered_Seurat.Rds"
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis.yaml"
  output:
    "/g/furlong/procaccia/organoids/analysis/seurat/SS196_{peakset}_filtered_Seurat_HFO_cells.Rds"
  log:
    "/g/furlong/procaccia/organoids/analysis/logs/split_HFO_cells_{peakset}.log"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  params:
    meta="protocol",
    group="HFO"
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/filter_metadata.R {input.seurat} {params.meta} {params.group} {output} > {log} 2>&1"

rule variable_features:
  input:
    seurat="/g/furlong/procaccia/organoids/analysis/seurat/SS196_{peakset}_filtered_Seurat{protocol}.Rds"
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis.yaml"
  output:
    "/g/furlong/procaccia/organoids/analysis/seurat/varfeat/SS196_{peakset}_varfeat_Seurat{protocol}.Rds"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  params:
    min_cutoff=config["MIN_CUTOFF"]
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/find_top_features.R --Seurat {input.seurat} --min_cutoff {params.min_cutoff} --out {output}"

rule TFIDF:
  input:
    "/g/furlong/procaccia/organoids/analysis/seurat/varfeat/SS196_{peakset}_varfeat_Seurat{protocol}.Rds"
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis.yaml"
  output:
    "/g/furlong/procaccia/organoids/analysis/seurat/TFIDF/SS196_{peakset}_TFIDF_Seurat{protocol}.Rds"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  log:
    "/g/furlong/procaccia/organoids/analysis/logs/TFIDF_{peakset}{protocol}.log"
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/calculate_TFIDF.R --Seurat {input} --out {output} > {log} 2>&1"
    

rule SVD:
  input:
    "/g/furlong/procaccia/organoids/analysis/seurat/TFIDF/SS196_{peakset}_TFIDF_Seurat{protocol}.Rds"
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis.yaml"
  output:
    "/g/furlong/procaccia/organoids/analysis/seurat/SVD/SS196_{peakset}_SVD_Seurat{protocol}.Rds"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  log:
    "/g/furlong/procaccia/organoids/analysis/logs/SVD_{peakset}{protocol}.log"
  params:
    svddims=config["SVD_NDIMS"]
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/calculate_SVD.R --Seurat {input} --ndim {params.svddims} --out {output} > {log} 2>&1 "


rule depth_cor:
  input:
    "/g/furlong/procaccia/organoids/analysis/seurat/SVD/SS196_{peakset}_SVD_Seurat{protocol}.Rds"
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis.yaml"
  output:
    "/g/furlong/procaccia/organoids/analysis/seurat/plots/SS196_{peakset}_filtered_Seurat{protocol}_depth_cor.png"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/plot_depth_cor.R {input} {output}"


rule calc_UMAP:
  input:
    "/g/furlong/procaccia/organoids/analysis/seurat/SVD/SS196_{peakset}_SVD_Seurat{protocol}.Rds"
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis.yaml"
  output:
    "/g/furlong/procaccia/organoids/analysis/seurat/UMAP/SS196_{peakset}_UMAP_Seurat{protocol}.Rds"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  log:
    "/g/furlong/procaccia/organoids/analysis/logs/calc_UMAP_{peakset}{protocol}.log"
  params:
    dims=config["UMAP"]["NDIMS"],
    reduction=config["UMAP"]["REDUCTION"]
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/calculate_UMAP.R --Seurat {input} --reduction {params.reduction} --dims {params.dims} --out {output} > {log} 2>&1"

rule silhouette:
  input:
    "/g/furlong/procaccia/organoids/analysis/seurat/UMAP/SS196_{peakset}_UMAP_Seurat{protocol}.Rds"
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis-clustering.yaml"
  output:
    f"/g/furlong/procaccia/organoids/analysis/seurat/plots/silhouette/{{peakset}}_peakset{{protocol}}/SS196_{{peakset}}_Seurat{{protocol}}_Silhouette_{paramspace.wildcard_pattern}.png"
  log:
    f"/g/furlong/procaccia/organoids/analysis/logs/silhouette/SS196_{{peakset}}_Seurat{{protocol}}_Silhouette_{paramspace.wildcard_pattern}.log"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  params:
    red=config["silhouette"]["RED"],
    dims=config["silhouette"]["DIMS"],
    simulation = paramspace.instance
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/plot_silhouette.R {input} {params.red} {params.dims} 1 {params.simulation[resolution]} {output} > {log} 2>&1"

rule clustering:
  input:
    "/g/furlong/procaccia/organoids/analysis/seurat/UMAP/SS196_{peakset}_UMAP_Seurat{protocol}.Rds"
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis-clustering.yaml"
  output:
    "/g/furlong/procaccia/organoids/analysis/seurat/clusters/SS196_{peakset}_clusters_Seurat{protocol}.Rds"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  log:
    "/g/furlong/procaccia/organoids/analysis/logs/clustering_{peakset}{protocol}.log"
  params:
    red=config["clustering"]["RED"],
    algo=config["clustering"]["ALGO"],
    res=get_resolution,
    dims=config["clustering"]["DIMS"]
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/calculate_clusters.R {input} {params.red} {params.dims} {params.algo} {params.res} {output} > {log} 2>&1"


rule plot_UMAP_clusters:
  input:
    "/g/furlong/procaccia/organoids/analysis/seurat/clusters/SS196_{peakset}_clusters_Seurat{protocol}.Rds"
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis.yaml"
  output:
    f"/g/furlong/procaccia/organoids/analysis/seurat/plots/SS196_{{peakset}}_Seurat{{protocol}}_UMAP_clusters.png"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  log:
    "/g/furlong/procaccia/organoids/analysis/logs/plot_UMAP_clusters_{peakset}{protocol}.log"
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/plot_UMAP.R {input} seurat.clusters {output} > {log} 2>&1"


rule plot_UMAP_protocol:
  input:
    "/g/furlong/procaccia/organoids/analysis/seurat/UMAP/SS196_{peakset}_UMAP_Seurat.Rds"
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis.yaml"
  output:
    "/g/furlong/procaccia/organoids/analysis/seurat/plots/SS196_{peakset}_filtered_Seurat_UMAP_protocol.png"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  params:
    group=config["GROUP"]
  shell:
     "Rscript /g/furlong/procaccia/organoids/src/R/plot_UMAP.R {input} {params.group} {output}"
     
rule calculate_gene_activity:
  input:
    "/g/furlong/procaccia/organoids/analysis/seurat/clusters/SS196_{peakset}_clusters_Seurat{protocol}.Rds"
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis.yaml"
  output:
    "/g/furlong/procaccia/organoids/analysis/seurat/gene_activity/SS196_{peakset}_gene_activity_Seurat{protocol}.Rds"
  log:
    "/g/furlong/procaccia/organoids/analysis/logs/calculate_gene_activity_{peakset}{protocol}.log"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/calculate_gene_activity.R --Seurat {input} --assay ATAC --activity ACTIVITY --out {output} > {log} 2>&1"
    
rule calculate_activity_markers:
  input:
    "/g/furlong/procaccia/organoids/analysis/seurat/gene_activity/SS196_{peakset}_gene_activity_Seurat{protocol}.Rds"
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis.yaml"
  output:
    "/g/furlong/procaccia/organoids/analysis/seurat/gene_activity/SS196_{peakset}_clusters_Seurat{protocol}_gene_activity_markers.tsv"
  log:
    "/g/furlong/procaccia/organoids/analysis/logs/calculate_gene_activity_markers_{peakset}{protocol}.log"
  resources:
    mem_mb=16000,
    time="0-00:30:00"
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/calculate_markers.R --Seurat {input} --out {output} --assay ACTIVITY --test_use 'wilcox' \
    --latent_vars 'nCount_ACTIVITY' > {log} 2>&1"

rule cistopic_training:
  input:
    "/g/furlong/procaccia/organoids/analysis/seurat/gene_activity/SS196_{peakset}_gene_activity_Seurat{protocol}.Rds"
  conda:
    "/g/furlong/procaccia/organoids/envs/atac-analysis-cistopic.yaml"
  resources:
    mem_mb=128000,
    cpus=25,
    time="0-10:00:00"
  log:
    "/g/furlong/procaccia/organoids/analysis/logs/cistopic_training_{peakset}{protocol}.log"
  params:
    ncpus=25,
    topics="2,5,10:30,40,60,100"
  output:
    "/g/furlong/procaccia/organoids/analysis/seurat/cistopic/SS196_{peakset}_cisTopicObject{protocol}.Rds"
  shell:
    "Rscript /g/furlong/procaccia/organoids/src/R/calculate_cistopic.R --Seurat {input} --out {output} --topics {params.topics} --ncpus {params.ncpus} --niter 1000 > {log} 2>&1"

























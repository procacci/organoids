# Script to get top features


############################## OPTIONS MENU ###################################

library("optparse")

# specify our desired options in a list
# by default OptionParser will add an help option equivalent to 
# make_option(c("-h", "--help"), action="store_true", default=FALSE, 
#               help="Show this help message and exit")

cat("\n")
option_list <- list(
  make_option("--Seurat", type="character", 
              help="[REQUIRED] Seurat object"),
  make_option("--min_cutoff", type="character", default="q5",
              help="[OPTIONAL] Number of singular values to compute [default=%default]"),
  make_option("--out", type="character", default="SeuratObj.Rds",
              help = "[OPTIONAL] Name of the RDS file that contains the Seurat object [default=%default]")
)

############################## PARSE OPTIONS ##################################


# get command line options, if help option encountered print help and exit,
# otherwise if options not found on command line then set defaults
opt <- parse_args(OptionParser(option_list=option_list))

if (is.null(opt$Seurat)) {
  write("Option --Seurat is required\nTry --help for help", stderr())
  q()
} else {
  SEURAT <- opt$Seurat
}

MINCUTOFF <- opt$min_cutoff

OUTFILE <- opt$out

################################# EXECUTION ###################################

library(Seurat)
library(Signac)

seurat.obj <- readRDS(SEURAT)

seurat.obj <- FindTopFeatures(seurat.obj,
                              min.cutoff = MINCUTOFF)

saveRDS(seurat.obj, file = OUTFILE)
q()


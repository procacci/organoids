# Script to run CountFragments and add it to the metadata of your Seurat object


############################## OPTIONS MENU ###################################

library("optparse")

# specify our desired options in a list
# by default OptionParser will add an help option equivalent to 
# make_option(c("-h", "--help"), action="store_true", default=FALSE, 
#               help="Show this help message and exit")

cat("\n")
option_list <- list(
  make_option("--Seurat", type="character", 
              help="[REQUIRED] Seurat object after running count_fragments.R"),
  make_option("--fragments", type="character", 
              help="[REQUIRED] Path to a fragments file"),
  make_option("--out", type="character", default="SeuratObj.Rds",
              help = "[OPTIONAL] Name of the RDS file that contains the Seurat object [default=%default]")
)

############################## PARSE OPTIONS ##################################


# get command line options, if help option encountered print help and exit,
# otherwise if options not found on command line then set defaults
opt <- parse_args(OptionParser(option_list=option_list))

if (is.null(opt$Seurat)) {
  write("Option --Seurat is required\nTry --help for help", stderr())
  q()
} else {
  SEURAT <- opt$Seurat
}

if (is.null(opt$fragments)) {
  write("Option --fragments is required\nTry --help for help", stderr())
  q()
} else {
  FRAGMENTS <- opt$fragments
}

OUTFILE <- opt$out


################################# EXECUTION ###################################

# Aggregate cells into clusters, calculate TF-IDF on clusters and saves it as a matrix

library(Seurat)
library(Signac)

# 1. Run the CountFragments function

cat("Running CountFragments...\n")

count.fragments <- CountFragments(fragments = FRAGMENTS,
                                  verbose = TRUE)

row.names(count.fragments) <- count.fragments$CB
count.fragments$CB <- NULL

cat("Done!\n\n")

# 2. Load the Seurat object

seurat.obj <- readRDS(SEURAT)

# 3. Attach count.fragments to the Seurat obj metadata

seurat.obj <- AddMetaData(seurat.obj, count.fragments)

# 4. Save and exit

saveRDS(seurat.obj, file = OUTFILE)
q()


#!/bin/bash
# script that takes as input a BAM file and returns a fragment file 
# 

Help()
{
   # Display Help
   echo "Construct a 10X Genomics Fragment file from a BAM file from sc(i)-ATAC-seq experiment"
   echo "Make sure environment /g/furlong/procaccia/organoids/envs/sinto is loaded before use"
   echo
   echo "Syntax: getFragments.sh [ -h | o <CHAR> | p <NUM> | b <CHAR> ]"
   echo "options:"
   echo "-h     Print this Help."
   echo "-o     Output fragment file name without extension"
   echo "-p     Number of CPUs to use"
   echo "-b     Input BAM file"
   echo
}

NPROC=1

while getopts o:p:b:h flag
do
    case "${flag}" in
        o) OUTFILE=${OPTARG};;
        p) NPROC=${OPTARG};;
        b) BAMFILE=${OPTARG};;
        h) Help;
          exit;;
    esac
done

echo "Running Sinto on file $BAMFILE"
sinto fragments -b $BAMFILE -f ${OUTFILE}.bed \
--barcode_regex "[^:]*" \
--collapse_within \
--nproc $NPROC \
--use_chrom "(?i)^[1-9XY]*"
echo
echo
echo "Done!"

echo "Sorting and compressing data..."
sort -k 1,1 -k2,2n ${OUTFILE}.bed | \
sed 's/^/chr/' | \
bgzip > ${OUTFILE}.bed.gz

tabix -p bed ${OUTFILE}.bed.gz
echo
echo "Done!"


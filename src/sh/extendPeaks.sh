#!/bin/bash
# script that takes as input a BED file and extend each read by d base pairs on each side

Help()
{
   # Display Help
   echo "Extend peaks by a determined size."
   echo "Make sure module BEDTools/2.30.0-GCC-11.2.0 is loaded before use"
   echo
   echo "Syntax: extendPeaks.sh [ -h | d <NUM> | o <CHAR> ] bedfile "
   echo "options:"
   echo "-h     Print this Help."
   echo "-d     Extension size (added to both sides)"
   echo "-o     Output BED file name"
   echo
}

while getopts d:o:h flag
do
    case "${flag}" in
        d) DISTANCE=${OPTARG};;
        o) OUTFILE=${OPTARG};;
        h) Help;
          exit;;
    esac
done

head /g/furlong/procaccia/organoids/hg38.genome

bedtools slop -g "/g/furlong/procaccia/organoids/hg38.genome" -i $5  -b $DISTANCE | bedtools merge > $OUTFILE;

#!/bin/sh
#SBATCH -A furlong                # group to which you belong
#SBATCH -J MACS2EML
#SBATCH -N 1                        # number of nodes
#SBATCH -n 8                        # number of cores
#SBATCH --mem 164G                    # memory pool for all cores
#SBATCH -t 0-10:00:00                   # runtime limit (D-HH:MM:SS)
#SBATCH -o /g/furlong/procaccia/organoids/analysis/MACS2/logs/slurm.%N.%j.MACS2_SS196_EMLOC_std_pc.out          # STDOUT
#SBATCH -e /g/furlong/procaccia/organoids/analysis/MACS2/logs/logs/slurm.%N.%j.MACS2_SS196_EMLOC_std_pc.err          # STDERR
#SBATCH --mail-type=END,FAIL        # notifications for job done & fail
#SBATCH --mail-user=simone.procaccia@embl.de # send-to address

module load Anaconda3

conda activate /g/furlong/procaccia/organoids/envs/MACS2

cp /g/tier2/furlong/secchia/organoids/SS196/bam/SS196.EMLOC.bam /scratch/procaccia/SS196.EMLOC.bam
mkdir -p /g/furlong/procaccia/organoids/analysis/MACS2/SS196_EMLOC


BAMFILE=/scratch/procaccia/SS196.EMLOC.bam
OUTPUTNAME=/scratch/procaccia/SS196_EMLOC_std_pc
OUTPUTDIR=/g/furlong/procaccia/organoids/MACS2/SS196_EMLOC

macs2 callpeak -t $BAMFILE \
-n $OUTPUTNAME \
--outdir $OUTPUTDIR \
--nomodel \
--keep-dup all \
--extsize 200 \
--shift -100 #2> /g/furlong/procaccia/organoids/analysis/MACS2/logs/MACS2_SS196_EMLOC_std_pc.out

rm /scratch/procaccia/SS196.EMLOC.bam


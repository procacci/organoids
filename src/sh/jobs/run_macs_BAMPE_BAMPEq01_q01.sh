#!/bin/sh

sbatch procaccia/organoids/src/sh/MACS2_SS196_Cardioids_BAMPE_pc.sh
sbatch procaccia/organoids/src/sh/MACS2_SS196_EMLOC_BAMPE_pc.sh
sbatch procaccia/organoids/src/sh/MACS2_SS196_HFO_BAMPE_pc.sh

sbatch procaccia/organoids/src/sh/MACS2_SS196_Cardioids_q01_pc.sh
sbatch procaccia/organoids/src/sh/MACS2_SS196_EMLOC_q01_pc.sh
sbatch procaccia/organoids/src/sh/MACS2_SS196_HFO_q01_pc.sh

sbatch procaccia/organoids/src/sh/MACS2_SS196_Cardioids_BAMPE_q01_pc.sh
sbatch procaccia/organoids/src/sh/MACS2_SS196_EMLOC_BAMPE_q01_pc.sh
sbatch procaccia/organoids/src/sh/MACS2_SS196_HFO_BAMPE_q01_pc.sh
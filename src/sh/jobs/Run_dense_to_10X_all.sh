#!/bin/bash

# This was run on Seneca

SCRIPT=/g/furlong/procaccia/organoids/src/R/dense_to_10X.R

Rscript $SCRIPT /g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/SS196_Cardioids_peaks_cells_matrix.tab /g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/Cardioids

Rscript $SCRIPT /g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/SS196_EMLOC_peaks_cells_matrix.tab /g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/EMLOC

Rscript $SCRIPT /g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/SS196_HFO_peaks_cells_matrix.tab /g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/HFO

Rscript $SCRIPT /g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/SS196_merged_BAMPE_peaks_cells_matrix.tab /g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/merged_BAMPE

Rscript $SCRIPT /g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/SS196_merged_BAMPE_q01_peaks_cells_matrix.tab /g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/merged_BAMPE_q01

Rscript $SCRIPT /g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/SS196_merged_q01_peaks_cells_matrix.tab /g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/merged_q01

Rscript $SCRIPT /g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/SS196_merged_custom_peaks_cells_matrix.tab /g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/merged_custom

Rscript $SCRIPT /g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/SS196_merged_std_peaks_cells_matrix.tab /g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/merged_std


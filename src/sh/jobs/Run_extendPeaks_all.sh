#!/bin/bash
#SBATCH -A furlong                # group to which you belong
#SBATCH -J peak_extension
#SBATCH -N 1                        # number of nodes
#SBATCH -n 1                        # number of cores
#SBATCH --mem 8G                     # memory pool for all cores
#SBATCH -t 0-10:00:00                   # runtime limit (D-HH:MM:SS)
#SBATCH -o /g/furlong/procaccia/organoids/analysis/MACS2/logs/slurm.%N.%j.peak_extension.out  # STDOUT
#SBATCH -e /g/furlong/procaccia/organoids/analysis/MACS2/logs/slurm.%N.%j.peak_extension.err  # STDERR
#SBATCH --mail-type=END,FAIL        # notifications for job done & fail
#SBATCH --mail-user=simone.procaccia@embl.de # send-to address

# Extend all files by 150 bp on both sides

OUTDIR="/g/furlong/procaccia/organoids/analysis/MACS2"
EXTENDPEAKS="/g/furlong/procaccia/organoids/src/sh/extendPeaks.sh"

# module load BEDTools/2.30.0-GCC-11.2.0

# std
sh $EXTENDPEAKS -d 150 \
-o $OUTDIR/SS196_Cardioids/SS196_Cardioids_std_pc_summits_extended.bed \
$OUTDIR/SS196_Cardioids/SS196_Cardioids_std_pc_summits.bed

sh $EXTENDPEAKS -d 150 \
-o $OUTDIR/SS196_EMLOC/SS196_EMLOC_std_pc_summits_extended.bed \
$OUTDIR/SS196_EMLOC/SS196_EMLOC_std_pc_summits.bed

sh $EXTENDPEAKS -d 150 \
-o $OUTDIR/SS196_HFO/SS196_HFO_std_pc_summits_extended.bed \
$OUTDIR/SS196_HFO/SS196_HFO_std_pc_summits.bed


# q=0.1

sh $EXTENDPEAKS -d 150 \
-o $OUTDIR/SS196_Cardioids_q01/SS196_Cardioids_q01_pc_summits_extended.bed \
$OUTDIR/SS196_Cardioids_q01/SS196_Cardioids_q01_pc_summits.bed


sh $EXTENDPEAKS -d 150 \
-o $OUTDIR/SS196_EMLOC_q01/SS196_EMLOC_q01_pc_summits_extended.bed \
$OUTDIR/SS196_EMLOC_q01/SS196_EMLOC_q01_pc_summits.bed

sh $EXTENDPEAKS -d 150 \
-o $OUTDIR/SS196_HFO_q01/SS196_HFO_q01_pc_summits_extended.bed \
$OUTDIR/SS196_HFO_q01/SS196_HFO_q01_pc_summits.bed




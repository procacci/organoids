#!/bin/sh
#SBATCH -A furlong                # group to which you belong
#SBATCH -J MACS2Car
#SBATCH -N 1                        # number of nodes
#SBATCH -n 8                        # number of cores
#SBATCH --mem 64G                    # memory pool for all cores
#SBATCH -t 0-10:00:00                   # runtime limit (D-HH:MM:SS)
#SBATCH -o /g/furlong/procaccia/organoids/analysis/MACS2/logs/slurm.%N.%j.MACS2_SS196_Cardioids_std_pc.out          # STDOUT
#SBATCH -e /g/furlong/procaccia/organoids/analysis/MACS2/logs/slurm.%N.%j.MACS2_SS196_Cardioids_std_pc.err          # STDERR
#SBATCH --mail-type=END,FAIL        # notifications for job done & fail
#SBATCH --mail-user=simone.procaccia@embl.de # send-to address

module load MACS2/2.2.7.1-foss-2020a-Python-3.8.2

cp /g/tier2/furlong/secchia/organoids/SS196/bam/SS196.Cardioids.bam /scratch/procaccia/SS196.Cardioids.bam
mkdir -p /g/furlong/procaccia/organoids/analysis/MACS2/SS196_Cardioids


BAMFILE=/scratch/procaccia/SS196.Cardioids.bam
OUTPUTNAME=/scratch/procaccia/SS196_Cardioids_std_pc
OUTPUTDIR=/g/furlong/procaccia/organoids/MACS2/SS196_Cardioids

macs2 callpeak -t $BAMFILE \
-n $OUTPUTNAME \
--outdir $OUTPUTDIR \
--nomodel \
--keep-dup all \
--extsize 200 \
--shift -100 2> /g/furlong/procaccia/organoids/analysis/MACS2/logs/MACS2_SS196_Cardioids_std_pc.out

rm /scratch/procaccia/SS196.Cardioids.bam

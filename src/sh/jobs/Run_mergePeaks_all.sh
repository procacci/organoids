#!/bin/bash
#SBATCH -A furlong                # group to which you belong
#SBATCH -J peak_merging
#SBATCH -N 1                        # number of nodes
#SBATCH -n 1                        # number of cores
#SBATCH --mem 16G                    # memory pool for all cores
#SBATCH -t 0-10:00:00                   # runtime limit (D-HH:MM:SS)
#SBATCH -o /g/furlong/procaccia/organoids/analysis/MACS2/logs/slurm.%N.%j.peak_merging.out  # STDOUT
#SBATCH -e /g/furlong/procaccia/organoids/analysis/MACS2/logs/slurm.%N.%j.peak_merging.err  # STDERR
#SBATCH --mail-type=END,FAIL        # notifications for job done & fail
#SBATCH --mail-user=simone.procaccia@embl.de # send-to address


OUTDIR="/g/furlong/procaccia/organoids/analysis/MACS2"
MERGEPEAKS="/g/furlong/procaccia/organoids/src/sh/mergePeaks.sh"

module load BEDTools/2.30.0-GCC-11.2.0

# std
sh $MERGEPEAKS \
-o $OUTDIR/SS196_merged_std_pc_extended.bed \
$OUTDIR/SS196_Cardioids/SS196_Cardioids_std_pc_summits_extended.bed \
$OUTDIR/SS196_EMLOC/SS196_EMLOC_std_pc_summits_extended.bed \
$OUTDIR/SS196_HFO/SS196_HFO_std_pc_summits_extended.bed

# BAMPE
sh $MERGEPEAKS \
-o $OUTDIR/SS196_merged_BAMPE_pc.bed \
$OUTDIR/SS196_Cardioids_BAMPE/SS196_Cardioids_BAMPE_pc_peaks.narrowPeak \
$OUTDIR/SS196_EMLOC_BAMPE/SS196_EMLOC_BAMPE_pc_peaks.narrowPeak \
$OUTDIR/SS196_HFO_BAMPE/SS196_HFO_BAMPE_pc_peaks.narrowPeak

# BAMPE + q 0.1
sh $MERGEPEAKS \
-o $OUTDIR/SS196_merged_BAMPE_q01_pc.bed \
$OUTDIR/SS196_Cardioids_BAMPE_q01/SS196_Cardioids_BAMPE_q01_pc_peaks.narrowPeak \
$OUTDIR/SS196_EMLOC_BAMPE_q01/SS196_EMLOC_BAMPE_q01_pc_peaks.narrowPeak \
$OUTDIR/SS196_HFO_BAMPE_q01/SS196_HFO_BAMPE_q01_pc_peaks.narrowPeak

# q 0.1
sh $MERGEPEAKS \
-o $OUTDIR/SS196_merged_q01_pc_extended.bed \
$OUTDIR/SS196_Cardioids_q01/SS196_Cardioids_q01_pc_summits_extended.bed \
$OUTDIR/SS196_EMLOC_q01/SS196_EMLOC_q01_pc_summits_extended.bed \
$OUTDIR/SS196_HFO_q01/SS196_HFO_q01_pc_summits_extended.bed

# Custom mix (Cardioids std, EMLOC std, HFO q0.1)
sh $MERGEPEAKS \
-o $OUTDIR/SS196_merged_custom_pc.bed \
$OUTDIR/SS196_Cardioids/SS196_Cardioids_std_pc_summits_extended.bed \
$OUTDIR/SS196_EMLOC/SS196_EMLOC_std_pc_summits_extended.bed \
$OUTDIR/SS196_HFO_q01/SS196_HFO_q01_pc_summits_extended.bed
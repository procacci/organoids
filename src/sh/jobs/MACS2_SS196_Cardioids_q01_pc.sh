#!/bin/bash
#SBATCH -A furlong                # group to which you belong
#SBATCH -J MACS2Cardioids
#SBATCH -N 1                        # number of nodes
#SBATCH -n 1                        # number of cores
#SBATCH --mem 10G                    # memory pool for all cores
#SBATCH -t 0-10:00:00                   # runtime limit (D-HH:MM:SS)
#SBATCH -o /g/furlong/procaccia/organoids/analysis/MACS2/logs/slurm.%N.%j.MACS2_SS196_Cardioids_q01_pc.out  # STDOUT
#SBATCH -e /g/furlong/procaccia/organoids/analysis/MACS2/logs/slurm.%N.%j.MACS2_SS196_Cardioids_q01_pc.err  # STDERR
#SBATCH --mail-type=END,FAIL        # notifications for job done & fail
#SBATCH --mail-user=simone.procaccia@embl.de # send-to address

module load Anaconda3
source $HOME/.bashrc
conda activate /g/furlong/procaccia/organoids/envs/MACS2

mkdir -p /g/furlong/procaccia/organoids/analysis/MACS2/SS196_Cardioids_q01

BAMFILE=/scratch/procaccia/SS196.Cardioids.bam
OUTPUTNAME=/scratch/procaccia/SS196_Cardioids_q01_pc
OUTPUTDIR=/g/furlong/procaccia/organoids/analysis/MACS2/SS196_Cardioids_q01

macs2 callpeak -t $BAMFILE \
-n $OUTPUTNAME \
--outdir $OUTPUTDIR \
--nomodel \
--keep-dup all \
-q 0.1



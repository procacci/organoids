#!/bin/bash
#SBATCH -A furlong                # group to which you belong
#SBATCH -J fragments
#SBATCH -N 1                        # number of nodes
#SBATCH -n 8                        # number of cores
#SBATCH --mem 64G                    # memory pool for all cores
#SBATCH -t 0-10:00:00                   # runtime limit (D-HH:MM:SS)
#SBATCH -o /g/furlong/procaccia/organoids/analysis/fragments/logs/slurm.%N.%j.fragment_calculation.out          # STDOUT
#SBATCH -e /g/furlong/procaccia/organoids/analysis/fragments/logs/slurm.%N.%j.fragment_calculation.err          # STDERR
#SBATCH --mail-type=END,FAIL        # notifications for job done & fail
#SBATCH --mail-user=simone.procaccia@embl.de # send-to address

module load Anaconda3
source $HOME/.bashrc
conda activate /g/furlong/procaccia/organoids/envs/sinto

GETFRAGMENTS="/g/furlong/procaccia/organoids/src/sh/getFragments.sh"

sh $GETFRAGMENTS -b /scratch/procaccia/SS196.true.nodups.bam \
-o /g/furlong/procaccia/organoids/analysis/fragments/SS196_all_fragments \
-p 8
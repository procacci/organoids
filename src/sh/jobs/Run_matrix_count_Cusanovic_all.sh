#!/bin/bash
#SBATCH -A furlong                # group to which you belong
#SBATCH -J mat_cusanovic          # name of the job
#SBATCH -N 1                        # number of nodes
#SBATCH -n 8                        # number of cores
#SBATCH --mem 16G                    # memory pool for all cores
#SBATCH -t 0-10:00:00                   # runtime limit (D-HH:MM:SS)
#SBATCH -o /g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/logs/slurm.%N.%j.matrix_Cusanovic.out  # STDOUT
#SBATCH -e /g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/logs/slurm.%N.%j.matrix_Cusanovic.err  # STDERR
#SBATCH --mail-type=END,FAIL        # notifications for job done & fail
#SBATCH --mail-user=simone.procaccia@embl.de # send-to address

COUNTSCRIPT="/g/furlong/project/66_sciATAC_meso/src/fly-atac_SS/sc_atac_window_counter_SS.py"
PEAKSDIR="/g/furlong/procaccia/organoids/analysis/MACS2"
BAMFILE="/scratch/procaccia/SS196.true.nodups.bam"
CELLBARCODES="/scratch/procaccia/SS196.true.nodups.sample.auto.readdepth.cells.indextable.txt"
OUTDIR="/g/furlong/procaccia/organoids/analysis/matrix/Cusanovic/"

# module load Anaconda3/2022.05
source $HOME/.bashrc
conda activate /g/furlong/Reddington/software/conda-envs/sciATAC_meso/

# std
python2.7 $COUNTSCRIPT $BAMFILE $CELLBARCODES $PEAKSDIR/SS196_merged_std_pc_extended.bed \
$OUTDIR/SS196_merged_std_peaks_cells_matrix.tab True

# BAMPE
python2.7 $COUNTSCRIPT $BAMFILE $CELLBARCODES $PEAKSDIR/SS196_merged_BAMPE_pc.bed \
$OUTDIR/SS196_merged_BAMPE_peaks_cells_matrix.tab True

# BAMPE + q 0.1
python2.7 $COUNTSCRIPT $BAMFILE $CELLBARCODES $PEAKSDIR/SS196_merged_BAMPE_q01_pc.bed \
$OUTDIR/SS196_merged_BAMPE_q01_peaks_cells_matrix.tab True

# q 0.1
python2.7 $COUNTSCRIPT $BAMFILE $CELLBARCODES $PEAKSDIR/SS196_merged_q01_pc_extended.bed \
$OUTDIR/SS196_merged_q01_peaks_cells_matrix.tab True

# Cardioids
python2.7 $COUNTSCRIPT $BAMFILE $CELLBARCODES $PEAKSDIR/SS196_Cardioids/SS196_Cardioids_std_pc_summits_extended.bed \
$OUTDIR/SS196_Cardioids_peaks_cells_matrix.tab True

# EMLOC
python2.7 $COUNTSCRIPT $BAMFILE $CELLBARCODES $PEAKSDIR/SS196_EMLOC/SS196_EMLOC_std_pc_summits_extended.bed \
$OUTDIR/SS196_EMLOC_peaks_cells_matrix.tab True

# HFO
python2.7 $COUNTSCRIPT $BAMFILE $CELLBARCODES $PEAKSDIR/SS196_HFO/SS196_HFO_std_pc_summits_extended.bed \
$OUTDIR/SS196_HFO_peaks_cells_matrix.tab True

# Custom
python2.7 $COUNTSCRIPT $BAMFILE $CELLBARCODES $PEAKSDIR/SS196_merged_custom_pc.bed \
$OUTDIR/SS196_merged_custom_peaks_cells_matrix.tab True

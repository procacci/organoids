#!/bin/bash
#SBATCH -A furlong                # group to which you belong
#SBATCH -J sci-atac_pipeline
#SBATCH -N 1                        # number of nodes
#SBATCH -n 20                        # number of cores
#SBATCH --mem 128G                    # memory pool for all cores
#SBATCH -t 2-00:00:00                   # runtime limit (D-HH:MM:SS)
#SBATCH -o /g/furlong/procaccia/organoids/analysis/preproc/Cusanovic/logs/slurm.%N.%j.preprocessing.out          # STDOUT
#SBATCH -e /g/furlong/procaccia/organoids/analysis/preproc/Cusanovic/logs/slurm.%N.%j.preprocessing.err          # STDERR
#SBATCH --mail-type=END,FAIL        # notifications for job done & fail
#SBATCH --mail-user=simone.procaccia@embl.de # send-to address

module load Anaconda3/2022.05
module load Bowtie2/2.4.4-GCC-11.2.0
module load Trimmomatic/0.39-Java-1.8.0_112

source /home/procacci/.bashrc
conda activate /home/procacci/procaccia/organoids/envs/sci-atac/

SCRIPTPATH=procaccia/organoids/src/py/sci-ATAC_processing-main

PYTHON=$(which python2.7)

echo
echo "Using python: ${PYTHON}"
echo
echo "--------------------- sc_atac_fastq_fix_trim.py ------------------------"
echo
echo

READ1=/g/furlong/STOCKS/Data/Assay/sequencing/2022/2022-09-01-HNMYKBGXM/HNMYKBGXM_SS196_22s003900-1-1_Secchia_lane122s003900_1_sequence.txt.gz
READ2=/g/furlong/STOCKS/Data/Assay/sequencing/2022/2022-09-01-HNMYKBGXM/HNMYKBGXM_SS196_22s003900-1-1_Secchia_lane122s003900_2_sequence.txt.gz

mkdir -p /g/furlong/procaccia/organoids/analysis/preproc/Cusanovic/trimmed

OUTDIR=/g/furlong/procaccia/organoids/analysis/preproc/Cusanovic/trimmed
PREFIX=SS196_simo_atac_organoids

python2.7 $SCRIPTPATH/sc_atac_fastq_fix_trim.py -R1 $READ1 -R2 $READ2 -O $OUTDIR -P $PREFIX

mv procaccia/organoids/analysis/preproc/Cusanovic/trimmed/SS196_simo_atac_organoids.split.trimmomatic.log procaccia/organoids/analysis/preproc/Cusanovic/logs

echo
echo
echo "------------------------------------------------------------------------"
echo
echo

echo "------------------------------- bowtie2 --------------------------------"
echo
echo

cp /g/furlong/genome/H.sapiens/GRCh38/index/bowtie2/Homo_sapiens.GRCh38.dna.toplevel* /scratch/procaccia/

GENOME=/scratch/procaccia/Homo_sapiens.GRCh38.dna.toplevel
READ1=/g/furlong/procaccia/organoids/analysis/preproc/Cusanovic/trimmed/SS196_simo_atac_organoids.split.1.trimmed.paired.fastq.gz
READ2=/g/furlong/procaccia/organoids/analysis/preproc/Cusanovic/trimmed/SS196_simo_atac_organoids.split.2.trimmed.paired.fastq.gz

mkdir -p /g/furlong/procaccia/organoids/analysis/preproc/Cusanovic/bam

OUTBAM=/g/furlong/procaccia/organoids/analysis/preproc/Cusanovic/bam/SS196_simo_atac_organoids_trimmed_paired.bam

bowtie2 -p 20 -X 2000 -3 1 -x $GENOME -1 $READ1 -2 $READ2 | samtools view -bS - > $OUTBAM
echo
echo
echo "------------------------------------------------------------------------"
echo
echo

echo "----------------------------- filtering --------------------------------"
echo
echo

INBAM=/g/furlong/procaccia/organoids/analysis/preproc/Cusanovic/bam/SS196_simo_atac_organoids_trimmed_paired.bam
OUTBAM=/g/furlong/procaccia/organoids/analysis/preproc/Cusanovic/bam/SS196_simo_atac_organoids_trimmed_paired_filtered.bam

samtools view -h -f3 -F12 -q10 $INBAM | \
grep -v MT | \
grep -v CHR_ | \
grep -v KI | \
grep -v GL | \
grep -v _CTF_ | \
grep -v _AMBIG_ | \
samtools view -Su - | \
samtools sort -@ 20 - -T temp -o $OUTBAM; 
samtools index $OUTBAM
echo
echo
echo "------------------------------------------------------------------------"
echo
echo

echo "----------------------- sc_atac_true_dedup.py --------------------------"
echo
echo

INBAM=/g/furlong/procaccia/organoids/analysis/preproc/Cusanovic/bam/SS196_simo_atac_organoids_trimmed_paired_filtered.bam
OUTBAM=/g/furlong/procaccia/organoids/analysis/preproc/Cusanovic/bam/SS196_simo_atac_organoids_trimmed_paired_filtered_dedup.bam

python2.7 $SCRIPTPATH/sc_atac_true_dedup.py $INBAM $OUTBAM
samtools index $OUTBAM
echo
echo
echo "------------------------------------------------------------------------"
echo
echo









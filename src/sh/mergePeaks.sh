#!/bin/bash
# script that takes as input an array of BED files containing peak sets, 
# merges reads that are overlapping (or adjacent)

Help()
{
   # Display Help
   echo "Merge peak sets into a single, merged BED file."
   echo "Make sure module BEDTools/2.30.0-GCC-11.2.0 is loaded before use"
   echo
   echo "Syntax: mergePeaks.sh [ -h | o <CHAR> ] bed1 ... bedN "
   echo "options:"
   echo "-h     Print this Help."
   echo "-o     Output BED file name"
   echo
}

while getopts o:h flag
do
    case "${flag}" in
        o) OUTFILE=${OPTARG};;
        h) Help;
          exit;;
    esac
done

if [ $# -le 1 ]; then
  echo "Please provide 2 or more files to merge.\n\n"
  exit;
else
  wc -l "${@:3}";
  cat "${@:3}" | \
  sort -k1,1 -k2,2n | \
  bedtools merge > $OUTFILE;
fi